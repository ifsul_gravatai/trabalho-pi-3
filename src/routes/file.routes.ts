import { Router } from "express"
import { httpHandler } from "@middlewares/handler.middleware"
import { fileController } from "@controllers/file.contoller"
import { deleteFileSchema } from "@middlewares/validators/file/deleteFile.schema"
import { downloadMiddleware } from "@middlewares/download.middleware"

const fileRoutes = Router()

fileRoutes.get('/:id/download', downloadMiddleware)
fileRoutes.delete('/:id', httpHandler(fileController.delete, deleteFileSchema))

export { fileRoutes }