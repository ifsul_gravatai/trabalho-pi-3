import { UserCreate } from "@repositories/user/dto/userCreate";

export interface SigninDTO extends UserCreate { }