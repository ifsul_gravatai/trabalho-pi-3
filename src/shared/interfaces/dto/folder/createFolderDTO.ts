import { Authenticated } from "../auth/AuthPayload"

export interface CreateFolderDTO extends Authenticated {
    fatherFolderId: number
    folderName: string
}