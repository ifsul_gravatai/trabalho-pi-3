import { Authenticated } from "../auth/AuthPayload";

export interface DetailsFolderRequestDTO extends Authenticated {
    path: string
}