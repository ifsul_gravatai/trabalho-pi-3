import { FileItem } from "./fileItem";
import { FolderItem } from "./folderItem";

export interface ListFolderItems {
    id: number
    folders: FolderItem[]
    files: FileItem[]
}