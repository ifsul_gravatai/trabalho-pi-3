export interface FolderItem {
    id: number,
    name: string,
    path?: string,
    updatedAt: Date
}