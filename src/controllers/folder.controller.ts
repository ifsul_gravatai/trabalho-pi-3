import { FileType } from "@core/enums/fileType";
import { HttpCode } from "@core/enums/httpCode";
import { HttpResponse } from "@core/interfaces/httpMethod";
import { FolderMessages } from "@core/messages/folderMessages";
import { File } from "@entity/File";
import { Folder } from "@entity/Folder";
import { fileRepository } from "@repositories/file/fileRepository";
import { CreateFolder } from "@repositories/folder/dto/createFolder";
import { folderRepository } from "@repositories/folder/folderRepository";
import { CreateFolderDTO } from "@shared/interfaces/dto/folder/createFolderDTO";
import { DeleteFolderDTO } from "@shared/interfaces/dto/folder/deleteFolderDTO";
import { DetailsFolderRequestDTO } from "@shared/interfaces/dto/folder/detailsFolderRequestDTO";
import { FileItem } from "@shared/interfaces/dto/folder/fileItem";
import { FileUploadDTO } from "@shared/interfaces/dto/folder/fileUploadDTO";
import { FolderItem } from "@shared/interfaces/dto/folder/folderItem";
import { ListFolderItems } from "@shared/interfaces/dto/folder/listFolderItems";
import { RenameFolderDTO } from "@shared/interfaces/dto/folder/renameFolderDTO";
import { getFileType } from "@util/fileExtention";
import { unlink } from "fs";
import { join } from "path";
import { QueryFailedError } from "typeorm";

class FolderController {

    public async create(input: CreateFolderDTO): Promise<HttpResponse> {
        const { fatherFolderId, folderName, userId } = input

        const fatherFolder = await folderRepository().findByIdAndUserId(fatherFolderId, userId)

        if (!fatherFolder) {
            return { statusCode: HttpCode.NOT_FOUND, json: { message: FolderMessages.NOT_FOUND_FATHER } }
        }

        const path = `${fatherFolder.path || ''}/${folderName}`

        const newFolder: CreateFolder = {
            userId,
            name: folderName,
            path,
            fatherFolderId
        }

        try {
            await folderRepository().create(newFolder)

            return { statusCode: HttpCode.CREATED }
        } catch (error) {
            if (!(error instanceof QueryFailedError)) {
                return { statusCode: HttpCode.INTERNAL_ERROR }
            }

            const errorCode: string = error.driverError.code
            const isConstraintError = errorCode === 'ER_DUP_ENTRY'

            if (isConstraintError) {
                return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: FolderMessages.FOLDER_ALREADY_EXIST } }
            }

            return { statusCode: HttpCode.INTERNAL_ERROR }
        }
    }

    public async details(input: DetailsFolderRequestDTO): Promise<HttpResponse<ListFolderItems>> {
        const { path, userId } = input

        const fatherPath = await folderRepository().findByPath(path, userId)

        if (!fatherPath) {
            return { statusCode: HttpCode.NOT_FOUND, json: { message: FolderMessages.NOT_FOUND_FATHER } }
        }

        const { id } = fatherPath

        const folders = await folderRepository().findChildrens(path, userId)
        const files = await fileRepository().findByFolderId(id)

        const mappedFolders = folders.map((folder): FolderItem => ({
            id: folder.id,
            name: folder.name,
            path: folder.path,
            updatedAt: folder.updatedAt
        }))

        const mappedFiles = files.map((file): FileItem => ({
            id: file.id,
            name: file.originalFilename,
            type: getFileType(file.originalFilename)
        }))

        const response: ListFolderItems = {
            id,
            folders: mappedFolders,
            files: mappedFiles
        }

        return { statusCode: HttpCode.OK, json: response }
    }

    public async rename(input: RenameFolderDTO): Promise<HttpResponse> {
        const { id, name, userId } = input

        const folder = await folderRepository().findById(id)

        if (!folder) {
            return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: FolderMessages.NOT_FOUND } }
        }

        if (folder.userId !== userId) {
            return { statusCode: HttpCode.FORBIDDEN }
        }

        folder.name = name

        await folderRepository().getInstance().save(folder)

        return { statusCode: HttpCode.OK }
    }

    public async delete(input: DeleteFolderDTO): Promise<HttpResponse> {
        const { folderId, userId } = input

        const baseFolder = await folderRepository().findByIdAndUserId(folderId, userId)

        await this.recursiveDelete(baseFolder)

        const files = await fileRepository().findByFolderId(baseFolder.id)
        files.forEach(this.deleteFile)
        await fileRepository().deleteByFolderId(baseFolder.id)
        await folderRepository().delete(baseFolder.id)

        return { statusCode: HttpCode.OK }
    }

    private async recursiveDelete(basefolder: Folder) {
        const folders = await basefolder.folders

        for (const folder of folders) {
            await this.recursiveDelete(folder)
            const files = await fileRepository().findByFolderId(folder.id)
            files.forEach(this.deleteFile)
            await fileRepository().deleteByFolderId(folder.id)
            await folderRepository().delete(folder.id)
        }
    }

    private deleteFile(file: File) {
        const filePath = join(__dirname, '../', 'uploads', file.filename)

        unlink(filePath, () => { })
    }
}

export const folderController = new FolderController()