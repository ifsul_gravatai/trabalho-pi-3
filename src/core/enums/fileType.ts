export enum FileType {
    IMG = 'IMG',
    PDF = 'PDF',
    DOC = 'DOC',
    VIDEO = 'VIDEO',
    ZIP = 'ZIP',
    COMPACT = 'COMPACT',
    // CODE = 'CODE',
    // CSV = 'CSV',
    // HTML = 'HTML',
    // ISO = 'ISO',
    UNKNOWN = 'UNKNOWN'
}