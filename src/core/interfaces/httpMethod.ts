import { HttpCode } from "@core/enums/httpCode"

export interface HttpResponse<T = {}, D = { message: string }> {
    statusCode: HttpCode
    json?: D | T
}

export type HttpMethod<T = any> = (input: T) => Promise<HttpResponse>