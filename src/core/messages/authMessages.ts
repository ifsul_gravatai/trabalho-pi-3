export enum AuthMessages {
    NO_SECRET_DEFINED = 'Chave para criptografica não definida',
    ERROR_ON_CREATE = 'Erro ao cadastrar usuário',
    USER_ALREADY_CREATED = 'Usuário já cadastrado',
    USER_NOT_FOUND = 'Usuário não encontrado',
    AUTH_FAILED = 'Falha ao autenticar o usuário',
    UNAUTHORIZED = 'Usuário não autorizado'
}