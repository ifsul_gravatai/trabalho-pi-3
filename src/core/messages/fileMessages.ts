export enum FileMessages {
    UPLOAD_STOPPED = 'O upload do arquivo foi cancelado',
    ERROR_ON_DELETE = 'Erro ao deletar arquivo',
    FILE_NOT_FOUND = 'Arquivo não encontrado'
}