import { SigninDTO } from '@shared/interfaces/dto/auth/signinDTO'
import joi from 'joi'

export const signinSchema = joi.object<SigninDTO>({
    name: joi.string().required(),
    email: joi.string().email().required(),
    password: joi.string().min(6).required(),
})