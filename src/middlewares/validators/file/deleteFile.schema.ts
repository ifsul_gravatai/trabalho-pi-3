import { DeleteFileDTO } from '@shared/interfaces/dto/file/deleteFileDTO'
import joi from 'joi'

export const deleteFileSchema = joi.object<DeleteFileDTO>({
    id: joi.number().positive().required(),
    userId: joi.number().positive().required()
})