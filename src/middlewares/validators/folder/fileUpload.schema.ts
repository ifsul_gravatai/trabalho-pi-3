import { FileUploadDTO } from '@shared/interfaces/dto/folder/fileUploadDTO'
import joi from 'joi'

export const fileUploadSchema = joi.object<FileUploadDTO>({
    filename: joi.string().required(),
    originalFilename: joi.string().required(),
    size: joi.number().positive().required(),
    folderId: joi.number().positive().required()
})