import { DeleteFolderDTO } from '@shared/interfaces/dto/folder/deleteFolderDTO'
import joi from 'joi'

export const deleteFolderSchema = joi.object<DeleteFolderDTO>({
    folderId: joi.number().positive().required()
})