import { DetailsFolderRequestDTO } from '@shared/interfaces/dto/folder/detailsFolderRequestDTO'
import joi from 'joi'

export const detailsFolderRequestSchema = joi.object<DetailsFolderRequestDTO>({
    path: joi.string().allow('')
})