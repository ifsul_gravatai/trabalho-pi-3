import { NextFunction, Request, Response } from "express";
import { join } from "path";
import { fileRepository } from "@repositories/file/fileRepository";
import { DeleteFileDTO } from "@shared/interfaces/dto/file/deleteFileDTO";
import { FileMessages } from "@core/messages/fileMessages";

export const downloadMiddleware = async (request: Request, response: Response, next: NextFunction): Promise<any> => {
    const { params, query, body } = request

    const input: DeleteFileDTO = {
        ...body,
        ...query,
        ...params,
    }

    const { id, userId } = input

    const file = await fileRepository().findByIdAndUser(id, userId)

    if (!file) {
        return response.status(500).json({ message: FileMessages.FILE_NOT_FOUND })
    }

    try {
        const filePath = join(__dirname, '../', 'uploads', file.filename)

        response.setHeader('Access-Control-Expose-Headers', 'Content-Disposition')

        return response.download(filePath, file.originalFilename)
    } catch (error) {
        return response.status(501).json({ input })
    }



}