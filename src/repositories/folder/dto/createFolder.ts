export interface CreateFolder {
    userId: number
    path: string
    name: string,
    fatherFolderId?: number
}