import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, Unique } from "typeorm"
import { Audit } from "./Audit"
import { Folder } from "./Folder"
import { User } from "./User"

@Entity()
@Unique('file_UNIQUE', ['originalFilename', 'folderId'])
export class File extends Audit {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar', nullable: false, unique: true })
    filename: string

    @Column({ type: 'varchar', nullable: false })
    originalFilename: string

    @Column({ type: 'int', nullable: false })
    @JoinColumn()
    userId: number

    @Column({ type: 'int', nullable: false })
    @JoinColumn()
    folderId: number

    @ManyToOne(() => User, user => user.folders)
    @JoinColumn()
    user: Promise<User>

    @ManyToOne(() => Folder, folder => folder.folders)
    @JoinColumn()
    folder: Promise<Folder>

}