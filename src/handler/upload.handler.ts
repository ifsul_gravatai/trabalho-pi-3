import { IncomingHttpHeaders } from "http";
import Busboy, { FileInfo } from 'busboy'
import { Readable } from "stream";
import { join } from 'path'
import { pipelineAsync } from "@util/util";
import { createWriteStream, rm } from 'fs'
import { io } from "src/config/bootstrap";
import { randomUUID } from "crypto";
import { NextFunction } from "express";

type OnFinishCallback = (filename: string, originalFilename: string, size: number) => NextFunction;

class UploadHandler {
    private size = 0

    constructor(private readonly socketId: string) { }

    registerEvents(headers: IncomingHttpHeaders, onFinish: OnFinishCallback) {
        const busboy = Busboy({ headers })

        busboy.once('file', (fieldname: string, file: Readable, fileInfo: FileInfo) => {
            const { filename } = fileInfo

            const random = this.generateRandomName(filename)

            this.onFire(file, random)

            busboy.on('finish', () => {
                const next = onFinish(random, filename, this.size)

                next()
            })
        })

        return busboy;
    }

    private generateRandomName(filename: string): string {
        const lastDot = filename.lastIndexOf('.')

        const extension = filename.slice(lastDot)

        return `${randomUUID()}${extension}`
    }

    private handleFileBytes() {
        async function* handleData(data: Readable) {
            for await (const item of data) {
                this.size += item.length

                io.to(this.socketId).emit('file-upload', this.size)

                yield item
            }
        }

        return handleData.bind(this)
    }

    private async onFire(file: Readable, filename: string) {
        const saveFileTo = join(__dirname, '../', 'uploads', filename)

        await pipelineAsync(
            file,
            this.handleFileBytes.apply(this),
            createWriteStream(saveFileTo)
        )
    }
}

export { UploadHandler }