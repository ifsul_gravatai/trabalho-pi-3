import { auth } from "./auth";
import { promisify } from 'util'
import { pipeline } from 'stream'

const pipelineAsync = promisify(pipeline)

export {
    auth as AuthUtil,
    pipelineAsync,
    promisify
}
