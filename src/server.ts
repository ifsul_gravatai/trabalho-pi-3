import "reflect-metadata"
import * as dotenv from 'dotenv'
dotenv.config()

import { bootstrap } from "./config/bootstrap";
import { db } from "./data-source";
import { AddressInfo } from "net";

const port = process.env.PORT || 3000

db.initialize().then(() => {
    console.log(`Database connection established`);

    bootstrap.listen(port, () => {
        const { address, port } = bootstrap.address() as AddressInfo
        console.log(`Application running at http://${address}:${port}`);
    })
}).catch((err) => {
    console.log(`Database connection error`);
})